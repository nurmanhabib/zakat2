FROM php:5.6-fpm

# Install Composer
COPY --from=composer:1 /usr/bin/composer /usr/bin/composer

RUN apt-get update && apt-get install -y git libmcrypt-dev mariadb-client \
    zlib1g-dev \
    libzip-dev \
    unzip \
    && docker-php-ext-install mcrypt pdo_mysql zip

# Set Permissions
RUN chown -R www-data:www-data /var/www
RUN find /var/www -type d -print0 | xargs -0 chmod 755 \
    && find /var/www -type f -print0 | xargs -0 -I {} chmod 664 {}

WORKDIR /var/www
