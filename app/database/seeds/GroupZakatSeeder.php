<?php

class GroupZakatSeeder extends Seeder
{

    public function run()
    {
        DB::table('groups')->truncate();

        $group = Group::create([
            'tahun'             => 1435,
            'tgl_mulai'         => '2014-07-25',
            'tgl_selesai'       => '2014-07-27',
            'uang_per_jiwa'     => 22500,
            'logo'              => 'logo1435.png',
        ]);

        Group::create([
            'tahun'             => 1436,
            'tgl_mulai'         => '2015-07-14',
            'tgl_selesai'       => '2015-07-16',
            'uang_per_jiwa'     => 25000,
            'logo'              => 'logo1436.png',
        ]);

        DB::table('zakat')->update([
            'group_id'          => $group->id,
        ]);
    }

}