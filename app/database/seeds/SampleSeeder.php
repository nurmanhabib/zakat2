<?php

class SampleSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();

        $user = User::create(array(
            'username'          => 'faridan',
            'email'             => 'faridan@gmail.com',
            'password'          => Hash::make('password'),
            'first_name'        => 'Faridan',
            'last_name'         => 'Muridan Noto',
            'remember_token'    => '',
        ));
    }

}