-- Created at 1.7.2015 9:57 using David Grudl MySQL Dump Utility
-- Host: localhost:8000
-- MySQL Server: 5.5.27
-- Database: zakat2

SET NAMES utf8;
SET SQL_MODE='NO_AUTO_VALUE_ON_ZERO';
SET FOREIGN_KEY_CHECKS=0;
-- --------------------------------------------------------

DROP TABLE IF EXISTS `zakat`;

CREATE TABLE `zakat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nomer` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `prefix` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'bapak',
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rt` tinyint(4) NOT NULL,
  `jumlah_jiwa` tinyint(4) NOT NULL,
  `jenis` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'fitrah',
  `beras` double NOT NULL,
  `uang` int(11) NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `zakat_nomer_unique` (`nomer`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `zakat` (`id`, `nomer`, `prefix`, `nama`, `alamat`, `rt`, `jumlah_jiwa`, `jenis`, `beras`, `uang`, `keterangan`, `user_id`, `created_at`, `updated_at`) VALUES
(3,	'FH_20140725_001',	'bapak',	'Drs. Margono',	'Jagalan Beji PA I/495',	10,	1,	'fitrah',	0,	22500,	'',	1,	'2014-07-23 16:34:07',	'2014-07-25 16:34:07'),
(4,	'FH_20140725_002',	'ibu',	'Waginah',	'Jagalan Beji PA I/493',	10,	2,	'fitrah',	5,	0,	'',	1,	'2014-07-23 16:35:03',	'2014-07-25 16:35:03'),
(5,	'FH_20140725_003',	'bapak',	'Joko Suwarso',	'Jagalan Ledoksari PA I/47',	3,	2,	'fitrah',	5,	0,	'',	1,	'2014-07-24 16:35:47',	'2014-07-25 16:35:47'),
(6,	'FH_20140725_004',	'bapak',	'Dirgahayu/Bapak Suud',	'Jagalan Beji PA I/386',	8,	3,	'fitrah',	0,	68500,	'',	1,	'2014-07-24 16:36:56',	'2014-07-25 16:36:56'),
(7,	'FH_20140725_005',	'bapak',	'Endri Supardi',	'Jagalan Beji PA I/398',	7,	2,	'fitrah',	5,	0,	'',	1,	'2014-07-25 16:56:24',	'2014-07-25 16:56:24'),
(8,	'FH_20140725_006',	'ibu',	'Hj. Darin Nusroh dan Tukiyem',	'Jl. Beji 23 B',	15,	2,	'fitrah',	5,	0,	'',	1,	'2014-07-25 16:57:07',	'2014-07-25 17:19:51'),
(9,	'FH_20140725_007',	'bapak',	'HM. Mudhakir',	'Jl. Beji 23',	15,	3,	'fitrah',	7.5,	0,	'',	1,	'2014-07-25 16:57:51',	'2014-07-25 16:57:51'),
(10,	'FH_20140725_008',	'bapak',	'Bagus Prasetyo',	'Jagalan Beji PA I/',	8,	2,	'fitrah',	5,	0,	'',	1,	'2014-07-25 16:58:45',	'2014-07-25 16:58:45'),
(11,	'FH_20140725_009',	'ibu',	'Boniyem',	'Jl. Beji 10',	15,	3,	'fitrah',	7.5,	0,	'',	1,	'2014-07-25 16:59:37',	'2014-07-25 16:59:37'),
(12,	'FH_20140725_010',	'bapak',	'Abas Joni Wibowo',	'Jagalan Beji PA I/426',	13,	2,	'fitrah',	0,	45000,	'',	1,	'2014-07-25 17:05:22',	'2014-07-25 17:11:59'),
(13,	'FH_20140725_011',	'ibu',	'Aini Purnamawati',	'Jl. Jagalan 1',	9,	5,	'fitrah',	12.5,	0,	'',	1,	'2014-07-25 17:16:10',	'2014-07-25 17:16:10'),
(14,	'FH_20140725_012',	'ibu',	'Ngadiah',	'Jl. Jagalan 1',	9,	1,	'fitrah',	2.5,	0,	'',	1,	'2014-07-25 17:16:51',	'2014-07-25 17:16:51'),
(15,	'FH_20140725_013',	'bapak',	'Budi Sumarman',	'Jagalan Beji PA I/497',	10,	4,	'fitrah',	0,	90000,	'',	1,	'2014-07-25 17:22:29',	'2014-07-25 17:22:29'),
(16,	'SQ_20140725_001',	'bapak',	'Budi Sumarman',	'Jagalan Beji PA I/497',	10,	1,	'shodaqoh',	0,	10000,	'',	1,	'2014-07-25 17:23:26',	'2014-07-25 17:23:26'),
(17,	'FH_20140725_014',	'ibu',	'Siti Suhartini, Emi Indiati, Tri Joko, Annisa Desetiana',	'Jagalan PA I/443',	9,	4,	'fitrah',	0,	90000,	'',	1,	'2014-07-25 17:26:58',	'2014-07-25 17:26:58'),
(18,	'SQ_20140725_002',	'bapak',	'Siti Suhartini, Emi Indiati, Tri Joko, Annisa Desetiana',	'Jagalan Beji PA I/443',	9,	1,	'shodaqoh',	0,	10000,	'',	1,	'2014-07-25 17:27:52',	'2014-07-25 17:27:52'),
(19,	'MA_20140726_001',	'bapak',	'Habib Nurrahman',	'Pakualaman',	10,	2,	'maal',	0,	45000,	'',	1,	'2014-07-26 14:53:43',	'2014-07-26 14:53:43');


-- THE END
