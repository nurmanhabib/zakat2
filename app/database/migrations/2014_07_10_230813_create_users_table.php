<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('username', 16)->unique();
			$table->string('email', 255)->unique();
			$table->string('password', 64);
			$table->string('first_name', 64);
			$table->string('last_name', 64)->nullable();
			$table->string('phone_number', 14)->nullable();
            $table->string('remember_token', 100);
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}