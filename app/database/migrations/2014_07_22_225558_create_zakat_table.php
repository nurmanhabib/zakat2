<?php

use Illuminate\Database\Migrations\Migration;

class CreateZakatTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('zakat', function($table){
			$table->increments('id');
			$table->string('nomer', 15)->unique();
			$table->string('prefix', 10)->default('bapak');
			$table->string('nama');
			$table->string('alamat');
			$table->tinyInteger('rt');
			$table->tinyInteger('jumlah_jiwa');
			$table->string('jenis', 50)->default('fitrah');
			$table->double('beras');
			$table->integer('uang');
			$table->string('keterangan');
			$table->integer('user_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('zakat');
	}

}