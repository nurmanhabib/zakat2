<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCustomRateZakat extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('zakat', function(Blueprint $table)
		{
			$table->integer('custom_rate')->default(0)->after('uang');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('zakat', function(Blueprint $table)
		{
			$table->dropColumn('custom_rate');
		});
	}

}
