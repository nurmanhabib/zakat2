<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('groups', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('tahun')->unique();
			$table->date('tgl_mulai');
			$table->date('tgl_selesai');
			$table->integer('uang_per_jiwa');
			$table->string('logo');
			$table->text('description')->nullable();
			$table->timestamps();
		});

		Schema::table('zakat', function(Blueprint $table)
		{
			$table->integer('group_id')->after('keterangan')->default(1);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('groups');
		Schema::table('zakat', function(Blueprint $table)
		{
			$table->dropColumn('group_id');
		});
	}

}
