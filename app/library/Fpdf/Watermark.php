<?php

class Watermark extends Rotation
{
    function Header()
    {
        //Put the watermark
        $this->SetFont('Arial','B',80);
        $this->SetTextColor(255,192,203);
        $this->RotatedText(60,90,'C O P Y',20);
    }

    function RotatedText($x, $y, $txt, $angle)
    {
        //Text rotated around its origin
        $this->Rotate($angle,$x,$y);
        $this->Text($x,$y,$txt);
        $this->Rotate(0);
    }
}