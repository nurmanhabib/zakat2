<?php

namespace Nurmanhabib\Zakat;

class Zakat
{
    protected $app;

    protected $session;

    protected $config;

    protected $view;

    protected $model;

    protected $group;

    public function __construct($app, $model, $group)
    {
        $this->app      = $app;
        $this->session  = $app->session;
        $this->config   = $app->config;
        $this->view     = $app->view;
        $this->model    = $model;
        $this->group    = $group->tahun($this->session->get('tahun_zakat'));
    }

    public function group()
    {
        return $this->group;
    }

    public function rekapitulasi()
    {
        return new Rekapitulasi($this->model, $this->group);
    }

    public function laporan()
    {
        return new Laporan($this->model, $this->group);
    }

    public function toArray()
    {
        $data = [
            'group'     => $this->group,
            'ringkasan' => $this->laporan()->getRingkasanZakat(),
        ];

        return $data;
    }

    public function __toString()
    {
        return json_encode($this->toArray());
    }
}