<?php

namespace Nurmanhabib\Zakat;

use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;

class Rekapitulasi
{
    protected $model;

    protected $group;

    protected $filters;

    public function __construct($model, $group, $filters = [])
    {
        $this->model    = $model;
        $this->group    = $group;

        $this->filters = [
            'jenis'         => 'all',
            'start_date'    => $this->group->tgl_mulai->startOfDay(),
            'end_date'      => $this->group->tgl_selesai->endOfDay(),
        ];

        $this->filters = array_merge($this->filters, $filters);

        if (!($this->start_date instanceof Carbon))
            $this->filters['start_date']    = Carbon::parse($this->start_date)->startOfDay();

        if (!($this->end_date instanceof Carbon))
            $this->filters['end_date']      = Carbon::parse($this->end_date)->endOfDay();
    }

    public function toExcel()
    {
        $info_date = $this->start_date->format('d-m-Y') . ' - ' . $this->end_date->format('d-m-Y');

        Excel::create('Rekapitulasi Zakat Masjid Faridan M. Noto Tahun '.$this->group->tahun.' H ['.$info_date.']', function($excel)
        {
            if ($this->jenis == 'all') {
                $this->generateSheet($excel, 'fitrah');
                $this->generateSheet($excel, 'maal');
                $this->generateSheet($excel, 'fidyah');
                $this->generateSheet($excel, 'shodaqoh');
            } else {
                $this->generateSheet($excel, $this->jenis);
            }
        })
        ->store('xlsx')->export('xlsx');
    }

    protected function generateSheet($excel, $jenis = 'fitrah')
    {
        $difference = $this->start_date->diffInDays($this->end_date);

        if($difference > 0)
            $txt_ranges = $this->start_date->format('d-m-Y') . ' sd ' . $this->end_date->format('d-m-Y');
        else
            $txt_ranges = $this->start_date->format('d-m-Y');

        $excel->sheet($this->model->getTitle($jenis), function($sheet) use ($jenis, $txt_ranges)
        {
            // Paper setup
            $sheet->setOrientation('landscape');

            // Header
            $sheet->appendRow(1, array('PANITIA RAMADHAN '.$this->group->tahun.' H MASJID FARIDAN MURIDAN NOTO'));
            $sheet->appendRow(2, array('SEKSI ZAKAT FITRAH'));
            $sheet->appendRow(3, array('REKAPITULASI PENERIMAAN ZAKAT FITRAH'));
            $sheet->cell('A1:D3', function($cell){
                $cell->setFontWeight('bold');
            });
            $sheet->mergeCells('A1:D1');
            $sheet->mergeCells('A2:D2');
            $sheet->mergeCells('A3:D3');

            // Atur lebar kolom
            $sheet->setWidth(array(
                'A' => 5,   // No
                'B' => 11,  // Tanggal
                'C' => 17,  // Nomer Pembayaran
                'D' => 60,  // Nama
                'E' => 27,  // Alamat
                'F' => 5,   // RT
                'H' => 11,  // Jenis
                'I' => 15,  // Beras
                'J' => 15,  // Uang
                'K' => 15,  // Keterangan
            ));

            // Header informasi: 1. jenis
            $sheet->setCellValue('E1', 'Jenis');
            $sheet->setCellValue('F1', ':');
            $sheet->setCellValue('G1', $this->model->getTitle($jenis));
            $sheet->mergeCells('G1:K1');
            
            // Header informasi: 2. date ranges
            $sheet->setCellValue('E2', 'Rentang Waktu');
            $sheet->setCellValue('F2', ':');
            
            $sheet->setCellValue('G2', $txt_ranges);
            $sheet->mergeCells('G2:K2');

            // Body
            $sheet->appendRow(4, array(
                'No.',
                'Tanggal',
                'Nomer',
                'Nama',
                'Alamat',
                'RT',
                'Jml Jiwa',
                'Jenis',
                'Beras (kg)',
                'Uang (Rp)',
                'Keterangan',
            ));
            $sheet->setHeight(4, 20);
            $sheet->cell('A4:K4', function($cell){
                $cell->setFontWeight('bold');
                $cell->setValignment('center');
            });

            $sheet->setColumnFormat(array(
                // Beras (kg)
                'I' => '_(#,##0.00 [$kg]_);_(#,##0.00 [$kg]_);_("-"_);_(@_)',

                // Uang (Rp)
                'J' => '_([$Rp-421]* #,##0_);_([$Rp-421]* (#,##0);_("-"_);_(@_)'
            ));

            $color_jenis    = array(
                'fitrah'    => '#92D050',
                'maal'      => '#E6B8B7',
                'shodaqoh'  => '#8DB4E2',
                'fidyah'    => '#FABF8F',
            );

            $i = 1;
            $r = $i + 4; // 4 baris pertama digunakan untuk header

            $zakat = $this->model->jenis($jenis);
            $zakat->whereBetween('created_at', [$this->start_date, $this->end_date]);
            
            foreach($zakat->get() as $info) {
                $sheet->appendRow(array(
                    $i,
                    $info->created_at->format('d-m-Y'),
                    $info->nomer,
                    $info->full_name,
                    $info->alamat,
                    $info->rt,
                    $info->jumlah_jiwa,
                    ucfirst($info->jenis),
                    $info->beras,
                    $info->uang,
                    $info->keterangan,
                ));

                $sheet->cell('H' . $r, function($cell) use($info, $color_jenis){
                    $color  = array_key_exists($info->jenis, $color_jenis) ? $color_jenis[$info->jenis] : '#FFFFFF';
                    $cell->setBackground($color);
                });

                $i++; $r++;
            }

            $sheet->setBorder('A4:K' . ($r - 1), 'thin');

            // Footer
            $sheet->appendRow($r, array('J U M L A H'));
            $sheet->mergeCells('A' . $r . ':F' . $r);
            $sheet->cell('A' . $r . ':K' . $r, function($cell){
                $cell->setAlignment('right');
                $cell->setFontWeight('bold');
            });
            $sheet->setCellValue('G' . $r, $r > 5 ? '=SUM(G5:G' . ($r - 1) . ')' : 0); // Jumlah Jiwa
            $sheet->setCellValue('I' . $r, $r > 5 ? '=SUM(I5:I' . ($r - 1) . ')' : 0); // Jumlah Beras
            $sheet->setCellValue('J' . $r, $r > 5 ? '=SUM(J5:J' . ($r - 1) . ')' : 0); // Jumlah Uang
            $sheet->setBorder('A' . $r . ':K' . $r, 'thin');

            if ($jenis == 'fitrah')
                $this->tambahanSheetFitrah($sheet, $r);
        });
    }

    protected function tambahanSheetFitrah($sheet, $r)
    {
        $sheet->setColumnFormat(array(
            // Beras (kg)
            'J' => '_(#,##0.00 [$kg]_);_(#,##0.00 [$kg]_);_("-"_);_(@_)',

            // Uang (Rp)
            'E' => '_([$Rp-421]* #,##0_);_([$Rp-421]* (#,##0);_("-"_);_(@_)'
        ));

        $r_jumlah = $r;

        $r++;
        $r++;

        $sheet->setCellValue('E'.$r, 'Harga beras per kilo');

        $sheet->setCellValue('H'.$r, 'Konversi ke Beras');
        $sheet->setCellValue('J'.$r, '=SUM(J'.$r_jumlah.'/'.$this->group->beras_per_kilo.')');
        $sheet->mergeCells('H'.$r.':I'.$r.'');

        $r++;

        $sheet->setCellValue('E'.$r, $this->group->beras_per_kilo);

        $sheet->setCellValue('H'.$r, 'Jumlah Zakat Beras');
        $sheet->setCellValue('J'.$r, '=SUM(I'.$r_jumlah.')');
        $sheet->mergeCells('H'.$r.':I'.$r.'');

        $r++;

        $sheet->setCellValue('H'.$r, 'T O T A L  B E R A S');
        $sheet->setCellValue('J'.$r, '=SUM(J'.($r - 2).':J'.($r - 1).')');
        $sheet->mergeCells('H'.$r.':I'.$r.'');

        $sheet->cell('H' . $r . ':J' . $r, function($cell){
            $cell->setFontWeight('bold');
        });

    }

    public function __set($key, $value)
    {
        if ($key == 'start_date') {
            if (!($value instanceof Carbon))
                $value = Carbon::parse($value)->startOfDay();
        }

        elseif ($key == 'end_date') {
            if (!($value instanceof Carbon))
                $value = Carbon::parse($value)->endOfDay();
        }
     
        $this->filters[$key] = $value;
    }

    public function __get($key)
    {
        return array_get($this->filters, $key);
    }
}