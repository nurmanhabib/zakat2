<?php

namespace Nurmanhabib\Zakat;

use App;
use Fpdf;
use Zakat as Model;

class Laporan
{
    protected $model;

    protected $group;

    protected $ringkasan;

    public function __construct($model, $group)
    {
        $this->model = $model;
        $this->group = $group;

        $this->setRingkasanZakat();
    }

    public function setRingkasanZakat()
    {
        $this->ringkasan = [
            'total_beras_uang'  => $this->hitungBerasUang(),
            'total_semua_uang'  => $this->hitungSemuaUang(),
            'total_beras'       => $this->hitungBeras(),
            'total_uang'        => $this->hitungUang(),
            'konversi_ke_beras' => $this->hitungBerasUang(false),
            'total_maal'        => $this->hitungMaal(),
            'total_shodaqoh'    => $this->hitungShodaqoh(),
            'total_fidyah'      => $this->hitungFidyah(),
        ];
    }

    public function getRingkasanZakat()
    {
        return $this->ringkasan;
    }

    public function hitungBerasUang($gabung = true)
    {
        $zakat = $this->model->fitrah()->uang()->get();
        $total = $gabung ? $this->hitungBeras() : 0;

        foreach($zakat as $info)
            if ($info->custom_rate)
                $total += $info->uang / $info->custom_rate;
            else
                $total += $info->uang / $this->group->beras_per_kilo;

        return $total;
    }

    public function hitungSemuaUang()
    {
        $total = $this->hitungUang();
        $total += $this->hitungMaal();
        $total += $this->hitungFidyah();
        $total += $this->hitungShodaqoh();

        return $total;
    }

    public function hitungBeras()
    {
        $zakat = $this->model->fitrah()->beras()->get();
        $total = 0.0;

        foreach($zakat as $info)
            $total += $info->beras;

        return $total;
    }

    public function hitungUang()
    {
        $zakat = $this->model->fitrah()->uang()->get();
        $total = 0;

        foreach($zakat as $info)
            $total += $info->uang;

        return $total;
    }

    public function hitungMaal()
    {
        $zakat = $this->model->maal()->uang()->get();
        $total = 0;

        foreach($zakat as $info)
            $total += $info->uang;

        return $total;
    }

    public function hitungShodaqoh()
    {
        $zakat = $this->model->shodaqoh()->uang()->get();
        $total = 0;

        foreach($zakat as $info)
            $total += $info->uang;

        return $total;
    }

    public function hitungFidyah()
    {
        $zakat = $this->model->fidyah()->uang()->get();
        $total = 0;

        foreach($zakat as $info)
            $total += $info->uang;

        return $total;
    }

	public function toPDF()
	{		
        $zakat  = Model::first();
        $group  = App::make('zakat')->group();

        $fpdf   = new Fpdf;

        $fpdf->AddPage('P', 'A4');
        $fpdf->SetMargins(10, 10);
        $fpdf->SetAutoPageBreak(false, 10);
        $fpdf->SetTitle($zakat->nomer);

        $fpdf->Image(public_path('img/'.$group->logo), 10, 10, 0, 18); // path, x, y, w, h
        $fpdf->SetFont('Arial', 'B', 14);
        
        $fpdf->Cell(35, 6); // w, h, txt, border, ln, align, fill
        $fpdf->Cell(170, 6, 'PANITIA RAMADHAN '.$group->tahun.' H MASJID FARIDAN MURIDAN NOTO', 0, 1); // w, h, txt, border, ln, align, fill
        
        $fpdf->Cell(35, 6);
        $fpdf->Cell(170, 6, 'SEKSI ZAKAT FITRAH', 0, 1);
        
        $fpdf->SetFont('Arial', 'B', 10);
        
        $fpdf->Cell(35, 6);
        $fpdf->Cell(170, 6, 'Jl. Beji 10 Yogyakarta 55112', 0, 1);

        $fpdf->SetLineWidth(0.8);
        $fpdf->Line(10, 30, 200, 30); // x1, y1, x2, y2

        $fpdf->SetFont('Arial', 'BU', 12);

        $fpdf->Ln(5);
        $fpdf->Cell(190, 5, 'LAPORAN ZAKAT', 0, 1, 'C');
        $fpdf->Ln(5);

        $heading = function($text) use($fpdf)
        {
            $fpdf->Cell(95, 5, $text, 0, 1);
        };

        $key = function($text) use($fpdf)
        {
            $fpdf->Cell(10, 5);
            $fpdf->Cell(50, 5, $text, 0, 0);
        };

        $batas = function($append = '') use($fpdf)
        {            
            $fpdf->Cell(10, 5, ':' . $append);
        };

        $value = function($text) use($fpdf)
        {
            $fpdf->Cell(30, 5, $text, 0, 1, 'R');
        };

        $garis_hitung = function($operator = '+', $text) use($fpdf)
        {
            $fpdf->SetLineWidth(0.4);
            $fpdf->Line(73, $fpdf->getY() + 1, 110, $fpdf->getY() + 1);
            $fpdf->setY($fpdf->getY() - 2);
            $fpdf->Cell(105, 5, $operator, 0, 1, 'R');

            $fpdf->Cell(100, 5, $text, 0, 1, 'R');

            $fpdf->setY($fpdf->getY() + 2);
        };

        $fpdf->SetFont('Arial', '', 12);

        // ZAKAT FITRAH
        $heading('I. Zakat Fitrah');        
        $key('a. Berupa Uang');             $batas(' Rp');      $value(number_format($this->total_uang, 0, ',', '.'));
        $key('b. Konversi ke Beras');       $batas();           $value(number_format($this->konversi_ke_beras, 2, ',', '.') . ' kg');
        $key('c. Berupa Beras');            $batas();           $value(number_format($this->total_beras, 2, ',', '.') . ' kg');
        
                                                                $garis_hitung('+', number_format($this->total_beras_uang, 2, ',', '.') . ' kg');        
        $fpdf->Ln(3);

        // ZAKAT MAAL
        $heading('II. Zakat Maal');        
        $key('a. Berupa Uang');             $batas(' Rp');      $value(number_format($this->total_maal, 0, ',', '.')); 
        $fpdf->Ln(3);

        // FIDYAH
        $heading('III. Fidyah');        
        $key('a. Berupa Uang');             $batas(' Rp');      $value(number_format($this->total_fidyah, 0, ',', '.'));
        $fpdf->Ln(3);

        // SHODAQOH
        $heading('IV. Shodaqoh');        
        $key('a. Berupa Uang');             $batas(' Rp');      $value(number_format($this->total_shodaqoh, 0, ',', '.'));
        $fpdf->Ln(3);
        
        // FOOTER
        $fpdf->Ln(5);
        $fpdf->SetFont('Arial', '', 10);
        $fpdf->Cell(190, 5, 'Demikian laporan zakat fitrah yang dapat kami sampaikan.', 0, 1);

        $fpdf->Ln(3);
        $fpdf->Cell(150, 5);
        $fpdf->Cell(50, 5, 'Mengetahui,', 0, 1);
        
        $fpdf->Cell(150, 5);
        $fpdf->Cell(50, 5, 'Sie Zakat Fitrah', 0, 1);
        $fpdf->Ln(7);

        $fpdf->Cell(150, 5);
        $fpdf->Cell(50, 5, 'Ir. Sigit Hartoyo', 0, 1);

        $fpdf->Output($zakat->nomer . '.pdf', 'I');

        exit;
	}

    public function toArray()
    {
        return ['ringkasan' => $this->ringkasan];
    }

    public function __set($key, $value)
    {
        $this->ringkasan[$key] = $value;
    }

    public function __get($key)
    {
        return array_get($this->ringkasan, $key);
    }

    public function __toString()
    {
        return json_encode($this->toArray());
    }
}