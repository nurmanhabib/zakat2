<?php

namespace Nurmanhabib\Zakat;

use Illuminate\Support\ServiceProvider;

class ZakatServiceProvider extends ServiceProvider
{
    public function boot()
    {
        //
    }

    public function register()
    {
        $this->app->singleton('zakat', function($app)
        {
            $model = new \Zakat;
            $group = new \Group;

            return new Zakat($app, $model, $group);
        });
    }
}