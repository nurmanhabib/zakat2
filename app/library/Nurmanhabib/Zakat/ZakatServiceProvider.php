<?php

namespace Nurmanhabib\Zakat;

use Illuminate\Support\ServiceProvider;

class ZakatServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if (!$this->app->session->has('tahun_zakat')) {
            $tahun = $this->app->config->get('zakat.tahun_default');
            $this->app->session->put('tahun_zakat', $tahun);
        }

        $this->app->view->share('tahun_zakat', $this->app->session->get('tahun_zakat'));
    }

    public function register()
    {
        $this->app->singleton('zakat', function($app)
        {
            $model = new \Zakat;
            $group = new \Group;

            return new Zakat($app, $model, $group);
        });
    }
}