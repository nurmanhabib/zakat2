<?php
namespace Roolib;

class Formed {

    public function serializeAssoc($inputs)
    {
        $items  = array();

        foreach ($inputs as $item => $values)
        {
            foreach ($values as $id => $value)
            {
                $items[$id][$item]  = $value;
            }
        }

        return $items;
    }

}