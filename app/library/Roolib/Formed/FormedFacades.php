<?php
use Illuminate\Support\Facades\Facade;

class Formed extends Facade {

    protected static function getFacadeAccessor() { return 'formed'; }

}

App::bind('formed', function()
{
    return new \Roolib\Formed;
});