<!DOCTYPE html>
<html class="bg-black">
    <head>
        <meta charset="UTF-8">
        <title>
            @yield('page_title', !empty($page_title) ? $page_title : 'Login') | 
            @yield('page_title', !empty($site_title) ? $site_title : 'Site Page')
        </title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

        @section('css')
            <!-- bootstrap 3.0.2 -->
            {{ HTML::style(asset('css/bootstrap.min.css')) }}
            <!-- font Awesome -->
            {{ HTML::style(asset('css/font-awesome.min.css')) }}
            <!-- Theme style -->
            {{ HTML::style(asset('css/AdminLTE.css')) }}

            <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
              <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
            <![endif]-->
        @show

    </head>
    <body class="bg-black">

        <div class="form-box" id="login-box">

            {{ Message::show() }}

            <div class="header">Sign In</div>
            {{ Form::open(array('route' => 'login.submit')) }}
                <div class="body bg-gray">

                    <div class="form-group">
                        {{ Form::text('username', null, array('class' => 'form-control', 'placeholder' => 'Username')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password')) }}
                    </div>
                </div>
                <div class="footer">
                    <button type="submit" class="btn bg-olive btn-block">Sign me in</button>
                </div>
            {{ Form::close() }}

        </div>

        @section('js')
            <!-- jQuery 2.0.2 -->
            {{ HTML::script(asset('js/jquery-2.0.2.min.js')) }}
            <!-- Bootstrap -->
            {{ HTML::script(asset('js/bootstrap.min.js')) }} 
        @show    

    </body>
</html>