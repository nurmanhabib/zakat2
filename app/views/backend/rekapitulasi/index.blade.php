@extends('backend.index')

@section('content')
    <ul>
        @foreach($groups as $group)
            <li>{{ link_to(route('backend.rekapitulasi.tahun', $group->tahun), $group->tahun) }}</li>
        @endforeach
    </ul>
@endsection