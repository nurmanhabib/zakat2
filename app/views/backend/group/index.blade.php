@extends('backend.index')

@section('content')
{{ Button::create() }}

<hr>

<table class="table table-hover">
    <thead>
        <tr>
            <th>#</th>
            <th>Tahun</th>
            <th>Waktu Penerimaan</th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
    @forelse($groups as $group)
        <tr>
            <td>{{ $group->id }}</td>
            <td>{{ $group->tahun }}</td>
            <td><strong>{{ $group->tgl_mulai->format('d-M-Y') }}</strong> s/d <strong>{{ $group->tgl_selesai->format('d-M-Y') }}</strong></td>
            <td>{{ Button::edit($group->id) }} {{ Button::destroy($group->id) }} @if(!$group->tahun_current) {{ Button::button_link_icon(null, route('backend.group.change', $group->tahun), 'check', ['size' => 'xs', 'type' => 'success']) }} @endif</td>
        </tr>
    @empty
        <tr>
            <td colspan="4">Tidak ada group.</td>
        </tr>
    @endforelse
    </tbody>
</table>
@endsection