@extends('backend.index')

@section('content')
    <div class="row">
        <div class="col-lg-3 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-blue">
                <div class="inner">
                    <h3>
                        {{ round($total_beras_uang, 2) }}<sup style="font-size: 20px">kg</sup>
                    </h3>
                    <p>
                        Total Beras (termasuk dari Uang)
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
            </div>
        </div><!-- ./col -->

        <div class="col-lg-3 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-blue">
                <div class="inner">
                    <h3>
                        <sup style="font-size: 20px">Rp</sup>{{ number_format($total_semua_uang, 0, ',', '.') }}
                    </h3>
                    <p>
                        Total semua Uang
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-cash"></i>
                </div>
            </div>
        </div><!-- ./col -->

        <div class="col-lg-3 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-blue">
                <div class="inner">
                    <h3>
                        <sup style="font-size: 20px">Rp</sup>{{ number_format($total_uang_today, 0, ',', '.') }}
                    </h3>
                    <p>
                        Total Uang Hari Ini ({{ \Carbon\Carbon::today()->format('d-m-Y') }})
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-cash"></i>
                </div>
            </div>
        </div><!-- ./col -->
    </div><!-- /.row -->

    <div class="row">
        <div class="col-lg-3 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>
                        {{ $total_beras }}<sup style="font-size: 20px">kg</sup>
                    </h3>
                    <p>
                        Total Zakat Fitrah Beras
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
            </div>
        </div><!-- ./col -->

        <div class="col-lg-3 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>
                        <sup style="font-size: 20px">Rp</sup>{{ number_format($total_uang, 0, ',', '.') }}
                    </h3>
                    <p>
                        Total Zakat Fitrah Uang ({{ round($konversi_ke_beras, 2) }} kg)
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-cash"></i>
                </div>
            </div>
        </div><!-- ./col -->

        <div class="col-lg-3 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>
                        <sup style="font-size: 20px">Rp</sup>{{ number_format($total_maal, 0, ',', '.') }}
                    </h3>
                    <p>
                        Total Zakat Maal
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-cash"></i>
                </div>
            </div>
        </div><!-- ./col -->
    </div>

    <div class="row">
        <div class="col-lg-offset-3 col-lg-3 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-purple">
                <div class="inner">
                    <h3>
                        <sup style="font-size: 20px">Rp</sup>{{ number_format($total_shodaqoh, 0, ',', '.') }}
                    </h3>
                    <p>
                        Total Shodaqoh
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-cash"></i>
                </div>
            </div>
        </div><!-- ./col -->

        <div class="col-lg-3 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>
                        <sup style="font-size: 20px">Rp</sup>{{ number_format($total_fidyah, 0, ',', '.') }}
                    </h3>
                    <p>
                        Total Fidyah
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-cash"></i>
                </div>
            </div>
        </div><!-- ./col -->
    </div><!-- /.row -->
@endsection