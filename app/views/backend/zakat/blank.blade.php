@extends('backend.index')

@section('content')
{{ Form::open(array('route' => 'invoice.blank')) }}
<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			{{ Form::label('from', 'Mulai dari angka?') }}
			{{ Form::text('from', null, array('class' => 'form-control')) }}
		</div>
		<div class="form-group">
			{{ Form::label('count', 'Berapa lembar?') }}
			{{ Form::text('count', null, array('class' => 'form-control')) }}
		</div>
		<div class="form-group">
			{{ Form::label('copy', 'Nota copy carbon?') }}
			{{ Form::checkbox('copy', 'copy') }}
		</div>
		{{ Button::button('Generate!') }}
	</div>
</div>
{{ Form::close() }}
@endsection