@extends('backend.layout.default')

@section('navigator-links')
    {{ Navigator::links() }}
@endsection

@section('css')
@parent
    {{ HTML::style('css/typeahead.css') }}
@endsection

@section('js')
@parent
    {{ HTML::script('js/jquery.number.min.js') }}
    {{ HTML::script('bower_components/typeahead.js/dist/typeahead.jquery.min.js') }}
    {{ HTML::script('bower_components/typeahead.js/dist/typeahead.bundle.min.js') }}

    <script>
    $(document).ready(function(){
        var group_info;

        $.ajax({
            url: '{{ url('api/group/info') }}',
            type: 'GET',
            success: function(data) {
                setAturanZakat(data)
            }
        });

        function setAturanZakat(group_info) {
            var beras_per_jiwa  = 2.5
            var uang_per_jiwa   = group_info.uang_per_jiwa;

            $('#jumlah_jiwa').change(function(){
                var jumlah_jiwa    = $(this).val()
                var form_beras      = $('#beras')
                var form_uang       = $('#uang')

                form_beras.val(jumlah_jiwa * beras_per_jiwa)
                form_uang.val(jumlah_jiwa * uang_per_jiwa)
            })
        }

        $('#uang').number(true, 0, ',', '.')

        var zakat = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('nama'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            identify: function(obj) { return obj.nama },
            // url points to a json file that contains an array of country names, see
            // https://github.com/twitter/typeahead.js/blob/gh-pages/data/countries.json
            prefetch: '{{ url('api/zakat/all.json') }}'
        });

        $('#nama').typeahead({
            classNames: {
                hint: 'form-control ztt-hint'
            }
        }, {
            display: 'nama',
            source: zakat
        })

        $('#nama').bind('typeahead:select', function(ev, suggestion) {
            console.log('Selection: ' + suggestion.nama);
            console.log('Selection: ' + suggestion.uang);
            $('#alamat').val(suggestion.alamat)
            $('#rt').val(suggestion.rt)
            $('#jumlah_jiwa').val(suggestion.jumlah_jiwa)
            $('#jumlah_jiwa').trigger('change')
        });

    })
    </script>
@endsection

@section('content')
    {{ Form::model($zakat, array('route' => array('backend.zakat.update', $zakat->id), 'method' => 'PUT')) }}
    <div class="row">
        <div class="col-sm-6">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Informasi Warga</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                {{ Form::label('prefix') }}
                                {{ Form::select('prefix', array(
                                    'bapak'     => 'Bapak',
                                    'ibu'       => 'Ibu',
                                    'saudara'   => 'Saudara',
                                    'saudari'   => 'Saudari',
                                ), null, array('class' => 'form-control')) }}
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <div class="form-group">
                                {{ Form::label('nama') }}
                                {{ Form::text('nama', null, array('class' => 'form-control')) }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group">
                                {{ Form::label('alamat') }}
                                {{ Form::text('alamat', null, array('class' => 'form-control')) }}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                {{ Form::label('rt', 'RT') }}
                                {{ Form::text('rt', null, array('class' => 'form-control')) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Keterangan Tambahan</h3>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        {{ Form::label('keterangan') }}
                        {{ Form::textarea('keterangan', null, array('class' => 'form-control')) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="box box-warning">
                <div class="box-header">
                    <h3 class="box-title">Zakat</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                {{ Form::label('jenis') }}
                                {{ Form::select('jenis', array(
                                    'fitrah'    => 'Fitrah',
                                    'maal'      => 'Maal',
                                    'shodaqoh'  => 'Shodaqoh',
                                    'fidyah'    => 'Fidyah',
                                ), null, array('class' => 'form-control')) }}
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                {{ Form::label('jumlah_jiwa') }}
                                {{ Form::select('jumlah_jiwa', array(
                                    1 => 1,
                                    2 => 2,
                                    3 => 3,
                                    4 => 4,
                                    5 => 5,
                                    6 => 6,
                                    7 => 7,
                                    8 => 8,
                                    9 => 9,
                                    10 => 10
                                ), null, array('class' => 'form-control')) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box box-success">
                <div class="box-header">
                    <h3 class="box-title">Jenis Pembayaran</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                {{ Form::label('beras') }}
                                <div class="input-group">
                                    {{ Form::text('beras', null, array('class' => 'form-control')) }}
                                    <span class="input-group-addon">kg</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                {{ Form::label('uang') }}
                                <div class="input-group">
                                    <span class="input-group-addon">Rp</span>
                                    {{ Form::text('uang', null, array('class' => 'form-control')) }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('jenis_bayar') }}
                        {{ Form::select('jenis_bayar', array(
                            'beras' => 'Beras',
                            'uang'  => 'Uang'
                        ), $zakat->value_method, array('class' => 'form-control')) }}
                    </div>
                </div>
            </div>

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Action</h3>
                </div>
                <div class="box-body">
                    {{ Button::button_icon('Update', 'save', ['size' => 'lg', 'type' => 'success']) }}
                    {{ Button::button_link_icon('Cetak (PDF)', route('export.zakat', array($zakat->id)), 'print', array('size' => 'lg'), array('target' => '_blank')) }}
                    {{ Button::button_icon('Simpan sebagai Baru', 'plus', array('size' => 'lg', 'type' => 'warning'), array('name' => 'as_new', 'value' => 1)) }}
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}
@endsection