@extends('backend.index')

@section('css')
@parent
{{ HTML::style('css/datatables/dataTables.bootstrap.css') }}
<style>
    .beras { text-align: right; }
    /*.uang { text-align: right; }*/
    .rupiah-value { float: right; }
    .jumlah-jiwa { text-align: right; }
</style>
@endsection

@section('js')
@parent
{{ HTML::script('js/plugins/datatables/jquery.dataTables.min.js') }}
{{ HTML::script('js/plugins/datatables/dataTables.bootstrap.js') }}
<script>
    $(document).ready(function(){
        $('#rekapitulasi').dataTable({
            "order": [[0, "desc"]]
        })
    })
</script>
@endsection

@section('content')
    <table class="table table-hover table-striped table-bordered" id="rekapitulasi">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nomer</th>
                <th>Nama</th>
                <th>RT</th>
                <th>Jiwa</th>
                <th>Jenis</th>
                <th>Beras</th>
                <th>Uang</th>
                <th>Keterangan</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            @foreach($zakat as $info)
            <tr>
                <td>{{ $info->id }}</td>
                <td>{{ $info->nomer }}</td>
                <td>{{ $info->full_name }}<br>
                <small>{{ $info->alamat }}</small></td>
                <td>{{ $info->rt }}</td>
                <td>{{ $info->jumlah_jiwa }}</td>
                <td>{{ $info->jenis }}</td>
                <td>{{ $info->beras }}</td>
                <td>{{ $info->uang }}</td>
                <td>{{ $info->keterangan }}</td>
                <td>{{Button::edit($info->id, null, array('size' => 'xs')) }}
                {{ Button::destroy($info->id, null, array('size' => 'xs')) }}
                {{ Button::button_link_icon('', route('export.zakat', array($info->id)), 'print', array('size' => 'xs', 'icon_fw' => true), array('target' => '_blank')) }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
@endsection