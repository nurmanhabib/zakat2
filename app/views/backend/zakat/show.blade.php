@extends('backend.layout.default')

@section('navigator-links')
    {{ Navigator::links() }}
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-6">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Langkah selanjutnya...</h3>
                </div>
                <div class="box-body">
                    {{ Button::button_link_icon('Cetak (PDF)', route('export.zakat', array($zakat->id)), 'print', array('size' => 'md'), array('target' => '_blank')) }}
                    {{ Button::button_link_icon('Edit', route('backend.zakat.edit', array($zakat->id)), 'pencil', array('size' => 'md', 'type' => 'warning')) }}
                    {{ Button::button_link_icon('Transaksi Baru', route('backend.zakat.create'), 'plus', array('size' => 'md', 'type' => 'success')) }}
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}
@endsection