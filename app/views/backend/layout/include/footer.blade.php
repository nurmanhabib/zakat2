        @section('js')
            <!-- jQuery 2.0.2 -->
            {{ HTML::script('js/jquery-2.0.2.min.js') }}
            <!-- jQuery UI 1.10.3 -->
            {{ HTML::script('js/jquery-ui-1.10.3.min.js') }}
            <!-- Bootstrap -->
            {{ HTML::script('js/bootstrap.min.js') }}
        @show

        <!-- AdminLTE App -->
        {{ HTML::script('js/AdminLTE/app.js') }}

    </body>
</html>