@include('backend.layout.include.header')

<div class="wrapper row-offcanvas row-offcanvas-left">

    <!-- Left side column. contains the logo and sidebar -->
    @section('navigator')
        @include('backend.layout.include.navigator')
    @show

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">                
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                @yield('page_title', !empty($page_title) ? $page_title : 'Default Page')
                <small>Control panel</small>
            </h1>
            {{ Breadcrumbs::renderIfExists() }}
        </section>

        <!-- Main content -->
        <section class="content">
            @section('message')
                {{ Message::show() }}
            @show

            @section('content')
                Default content!
            @show
        </section><!-- /.content -->
    </aside><!-- /.right-side -->
</div>

@include('backend.layout.include.footer')