@extends('backend.index')

@section('js')
@parent
    <script>
    $(document).ready(function(){
        $('#ranges').daterangepicker({
            format: 'YYYY-MM-DD'
        })
    })
    </script>
@endsection

@section('content')
    {{ Form::open(array('route' => 'export.rekapitulasi')) }}
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    {{ Form::label('jenis') }}
                    {{ Form::select('jenis', array(
                        'all'       => 'Semua Jenis',
                        'fitrah'    => 'Fitrah',
                        'maal'      => 'Maal',
                        'shodaqoh'  => 'Shodaqoh',
                        'fidyah'    => 'Fidyah',
                    ), null, array('class' => 'form-control')) }}
                </div>
                {{ Form::label('ranges') }}
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                    {{ Form::text('ranges', null, array('class' => 'form-control')) }}
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    {{ Button::button_icon('Download (Excel)', 'download', array('size' => 'lg', 'icon_fw' => true, 'block' => true)) }}
                </div>
            </div>
        </div>
    {{ Form::close() }}
@endsection