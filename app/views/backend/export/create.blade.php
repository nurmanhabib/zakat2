@extends('backend.layout.default')

@section('navigator-links')
    {{ Navigator::links() }}
@endsection

@section('js')
@parent
    {{ HTML::script('js/jquery.number.min.js') }}

    <script>
    $(document).ready(function(){
        $('#jumlah_jiwa').change(function(){
            var jumlah_jiwa    = $(this).val()
            var beras_per_jiwa = 2.5
            var beras_per_kg   = 27000
            var beras = jumlah_jiwa * beras_per_jiwa
            var uang  = $.number(jumlah_jiwa * beras_per_kg, 0, ',', '.')

            var method_beras   = $('#method-beras')
            var method_uang    = $('#method-uang')

            method_beras.empty().html(beras + ' kg')
            method_uang.empty().html('Rp ' + uang)
        })

        $('#method').change(function(){
            if(this.value == 'uang')
            {
                $('#rp').removeClass('hide')
                $('#kg').addClass('hide')
            }
            else
            {
                $('#kg').removeClass('hide')
                $('#rp').addClass('hide')
            }
        })
    })
    </script>
@endsection

@section('content')
    {{ Form::open(array('route' => 'backend.maal.store')) }}
    {{ Form::hidden('type', 'maal') }}
    <div class="row">
        <div class="col-md-6">
            <div class="box box-success">
                <div class="box-header">
                    <h3 class="box-title">Informasi Pembayar</h3>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        {{ Form::label('first_name', 'Nama Lengkap') }}
                        {{ Form::text('first_name', null, array('class' => 'form-control')) }}
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group">
                                {{ Form::label('address[street]', 'Alamat') }}
                                {{ Form::text('address[street]', null, array('class' => 'form-control')) }}
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                {{ Form::label('address[rt]', 'RT') }}
                                {{ Form::text('address[rt]', null, array('class' => 'form-control')) }}
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                {{ Form::label('address[rw]', 'RW') }}
                                {{ Form::text('address[rw]', null, array('class' => 'form-control')) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Catatan Tambahan</h3>
                </div>
                <div class="box-body">
                    {{ Form::textarea('description', null, array('class' => 'form-control', 'rows' => 3, 'placeholder' => 'Tambahkan keterangan jika dibutuhkan')) }}
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="box box-success">
                <div class="box-header">
                    <h3 class="box-title">Zakat Maal</h3>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        {{ Form::label('jumlah_jiwa') }}
                        {{ Form::select('jumlah_jiwa', array(
                            1 => 1,
                            2 => 2,
                            3 => 3,
                            4 => 4,
                            5 => 5,
                            6 => 6,
                            7 => 7,
                            8 => 8,
                            9 => 9,
                            10 => 10
                        ), 1, array('class' => 'form-control')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('method', 'Berupa') }}
                        {{ Form::select('method', array(
                            'beras' => 'Beras',
                            'uang'  => 'Uang'
                        ), 'beras', array('class' => 'form-control')) }}
                    </div>
                        {{ Form::label('value', 'Jumlah Zakat Maal') }}
                    <div class="input-group">
                        <span class="input-group-addon hide" id="rp">Rp</span>
                        {{ Form::text('value', null, array('class' => 'form-control')) }}
                        <span class="input-group-addon" id="kg">kg</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Jenis Pembayaran</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <input type="submit" class="btn btn-lg btn-block btn-primary" value="Bayar Sekarang">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}
@endsection