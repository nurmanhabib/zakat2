@extends('backend.layout.default')

@section('navigator-links')
    {{ Navigator::links() }}
@endsection

@section('content')
    {{ Form::open(array('route' => 'backup')) }}
    <div class="row">
        <div class="col-md-6">
            <div class="box box-success">
                <div class="box-header">
                    <h3 class="box-title">Informasi Backup</h3>
                </div>
                <div class="box-body">
                    <p>Nama Database: {{ Config::get('database.connections.mysql.database') }}</p>
                    {{ Button::button_icon('Backup', 'laptop') }}
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}
@endsection