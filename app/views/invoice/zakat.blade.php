<!DOCTYPE html>
<html>
<head>
    <title>FH_20160702_050</title>

    <style type="text/css">
        @page {
            margin: 0.5cm;
        }

        body {
            font-family: sans-serif;
            font-size: 10pt;
        }

        h1, h3, p {
            margin: 0;
        }

        table {
            width: 100%;
        }

        h1 {
            font-size: 12pt;
        }

        h3 {
            font-size: 10pt;
        }

        .center {
            text-align: center;
        }

        .title {
            font-size: 12pt;
            text-decoration: underline;
        }

        .zakat-price {
            font-size: 14pt;
            font-weight: bold;
            text-align: center;
        }

        .zakat-detail {
            text-align: center;
            font-style: italic;
        }
    </style>
</head>
<body>
    <div>
        <table>
            <tr>
                <td width="15%"><img src="{{ public_path('img/logo1436.png') }}" width="100"></td>
                <td>
                    <h1>PANITIA RAMADHAN 1436 H MASJID FARIDAN MURIDAN NOTO<br>
                    SEKSI ZAKAT FITRAH</h1>
                    <h3>Jl. Beji 10 Yogyakarta 55112</h3>
                </td>
            </tr>
        </table>

        <hr>
    </div>

    <div>
        <h1 class="title center">TANDA TERIMA ZAKAT FITRAH</h1>
    </div>

    <div>
        <table>
            <tr>
                <td><strong>Nomer : FH_20150716_050</strong></td>
                <td align="right">Tanggal/Jam : 16-07-2015 23:21:37</td>
            </tr>
            <tr>
                <td colspan="2">
                    <p>Telah terima dari <strong>Bapak Bambang Susetyaningprang (3 jiwa)</strong></p>
                    <p>Alamat : Jagalan Beji PA I/470</p>
                </td>
            </tr>
        </table>

        <table>
            <tr>
                <td>
                    <p class="zakat-price">Zakat Fitrah berupa UANG Rp 75.000,00</p>
                    <p class="zakat-detail">(Tujuh puluh lima ribu rupiah)</p>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>