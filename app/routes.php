<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| FRONTEND ROUTES
|--------------------------------------------------------------------------
*/

// Route home public or first access to web
Route::get('/',             array('as'  => 'home', function(){
    return Redirect::route('dashboard');
}));

// Route login
Route::get('admin/login',   array('as'  => 'login',          'uses' => 'Backend\HomeController@getLogin'));
Route::post('admin/login',  array('as'  => 'login.submit',   'uses' => 'Backend\HomeController@postLogin'));

/*
|--------------------------------------------------------------------------
| BACKEND ROUTES
|--------------------------------------------------------------------------
*/

// Admin user must be authorization
Route::group(array('before' => 'auth', 'namespace' => 'Backend', 'prefix' => 'admin'), function(){

    // Dashboard Home
    Route::get('/',         array('as'  => 'dashboard',     'uses' => 'HomeController@index'));

    // Logout
    Route::get('logout',    array('as'  => 'logout',        'uses' => 'HomeController@getLogout'));

    // Transaksi Zakat Resource
    Route::resource('pembayaran', 'ZakatController', array(
        'names' => array(
            'index'     => 'backend.zakat',
            'create'    => 'backend.zakat.create',
            'store'     => 'backend.zakat.store',
            'show'      => 'backend.zakat.show',
            'edit'      => 'backend.zakat.edit',
            'update'    => 'backend.zakat.update',
            'destroy'   => 'backend.zakat.destroy',
        )
    ));

    Route::controller('rekapitulasi', 'RekapitulasiController', [
        'getIndex'      => 'backend.rekapitulasi.index',
        'getTahun'      => 'backend.rekapitulasi.tahun',
    ]);

    // Transaksi Zakat Maal Resource
    Route::controller('export', 'ExportController', array(
        'getIndex'          => 'export',
        'getZakat'          => 'export.zakat',
        'getRekapitulasi'   => 'export.rekapitulasi',
        'getBlank'          => 'invoice.blank',
        'getBackup'         => 'backup',
        'getLaporan'        => 'laporan',
    ));

    Route::get('group/change/{tahun}', ['as' => 'backend.group.change', 'uses' => 'GroupController@changeTahun']);

    Route::resource('group', 'GroupController', [
        'names' => [
            'index'     => 'backend.group.index',
            'create'    => 'backend.group.create',
            'store'     => 'backend.group.store',
            'show'      => 'backend.group.show',
            'edit'      => 'backend.group.edit',
            'update'    => 'backend.group.update',
            'destroy'   => 'backend.group.destroy',
        ]
    ]);

});

Route::get('backup', function(){
    $pdo = new mysqli('localhost', 'root', '', 'zakat2');
    $dump = new Mysqldump;
    return $dump::write();

    return Mysqldump::table('zakat')->write();

    return $dump->write();
    return $dump->table;
    return $dump->save('export2.sql.gz');
    //return Config::get('database.connections.mysql');
});

/*
|--------------------------------------------------------------------------
| API ROUTES
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => 'api'], function()
{
    Route::get('group/info', function()
    {
        $tahun_aktif = Session::get('tahun_zakat');
        $group = Group::tahun($tahun_aktif);

        return $group;
    });

    Route::get('zakat/all.json', function()
    {
        return Response::json(Zakat::semuaTahun()->get());
    });
});


/*
|--------------------------------------------------------------------------
| TESTING ROUTES
|--------------------------------------------------------------------------
*/

Route::get('tes', function()
{
    $rekap = App::make('zakat')->rekapitulasi();
    $rekap->jenis       = 'all';
    // $rekap->start_date  = '2014-07-25';
    // $rekap->end_date    = '2014-07-27';
    // $rekap->toExcel();

    $laporan = App::make('zakat')->laporan();
    $laporan->toPDF();
});

Route::get('fpdf', function()
{
    // return View::make('invoice.zakat');
    
    $pdf = App::make('dompdf');
    $pdf->loadView('invoice.zakat');
    // $pdf->loadHTML('<h1>Test</h1>');
    $pdf->setPaper([0, 0, 270.0, 612.0], 'landscape');

    return $pdf->stream();
});