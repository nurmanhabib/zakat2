<?php

class Group extends Eloquent
{
    protected $dates = ['tgl_mulai', 'tgl_selesai'];

    public function getTahunCurrentAttribute()
    {
        return Session::get('tahun_zakat') == $this->tahun;
    }

    public function getBerasPerKiloAttribute()
    {
        return $this->uang_per_jiwa / 2.5;
    }

    public function getRateAttribute()
    {
        return $this->beras_per_kilo;
    }

    public function zakat()
    {
        return $this->hasMany('Zakat');
    }

    public function scopeTahun($query, $tahun)
    {
        return $query->where('tahun', $tahun)->first();
    }
}