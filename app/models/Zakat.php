<?php

use Carbon\Carbon;
use Scopes\TahunanTrait;

class Zakat extends Eloquent
{
    use TahunanTrait;

    protected $table    = 'zakat';

    protected $aliases  = [
        'fitrah'    => 'Zakat Fitrah',
        'maal'      => 'Zakat Maal',
        'shodaqoh'  => 'Shodaqoh',
        'fidyah'    => 'Fidyah',
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function($data){
            $data->nomer    = $data->setNomer($data->jenis);
            $data->uang     = str_replace('.', '', $data->uang);

            $group          = Group::tahun(Session::get('tahun_zakat'));
            $data->group_id = $group->id;
            
            if(empty($data->user_id))
                $data->user_id  = Auth::user()->id;
        });

        static::updating(function($data){
            $data->uang     = str_replace('.', '', $data->uang);
        });
    }

    public function getFullNameAttribute()
    {
        return ucfirst($this->prefix) . ' ' . $this->nama;
    }

    public function getFullNameJiwaAttribute()
    {
        $full_name  = $this->full_name;

        if($this->jenis == 'fitrah')
            $full_name  .= ' (' . $this->jumlah_jiwa . ' jiwa)';

        return $full_name;
    }

    public function getValueFormatAttribute()
    {
        if($this->beras)
            $value  = 'BERAS ' . number_format($this->beras, 1, ',', '.') . ' kg';
        else
            $value  = 'UANG Rp ' . number_format($this->uang, 2, ',', '.');

        return $value;
    }

    public function getTerbilangAttribute()
    {
        return $this->uang ? toTerbilang($this->uang) . ' rupiah' : toTerbilang($this->beras) . ' kilogram';
    }

    public function getValueMethodAttribute()
    {
        if($this->beras)
            $value  = 'beras';
        else
            $value  = 'uang';

        return $value;
    }

    public function getTitleAttribute()
    {
        $aliases  = $this->aliases;

        return array_key_exists($this->jenis, $aliases) ? $aliases[$this->jenis] : ucfirst($this->jenis);
    }

    public function scopeNomer($query, $nomer)
    {
        return $query->where('nomer', '=', $nomer);
    }

    public function setNomer($jenis = 'fitrah', $i = 1)
    {
        $kode_prefix    = array(
            'fitrah'    => 'FH',
            'maal'      => 'MA',
            'shodaqoh'  => 'SQ',
            'fidyah'    => 'FD',
        );

        $nomer    = array_key_exists($jenis, $kode_prefix) ? $kode_prefix[$jenis] : 'AA';
        $nomer    .= date('_Ymd_');
        $nomer    .= sprintf('%03d', $i);

        $cekNomer  = Zakat::nomer($nomer);

        if($cekNomer->count())
            return $this->setNomer($jenis, $i+1);
            return $nomer;
    }

    public function group()
    {
        return $this->belongsTo('Group');
    }

    public function scopeGroupId($query, $group_id)
    {
        return $query->where('group_id', $group_id);
    }

    public function scopeJenis($query, $jenis)
    {
        return $query->where('jenis', '=', $jenis);
    }
    public function scopeFitrah($query)
    {
        return $query->jenis('fitrah');
    }

    public function scopeMaal($query)
    {
        return $query->jenis('maal');
    }

    public function scopeShodaqoh($query)
    {
        return $query->jenis('shodaqoh');
    }

    public function scopeFidyah($query)
    {
        return $query->jenis('fidyah');
    }

    public function scopeBeras($query)
    {
        return $query->where('beras', '>', 0);
    }

    public function scopeUang($query)
    {
        return $query->where('uang', '>', 0);
    }

    public function scopeTahun($query, $tahun)
    {
        $dt     = Carbon::create($tahun);
        $start  = $dt->copy()->startOfYear();
        $end    = $dt->copy()->endOfYear();

        return $query->where('created_at', '>=', $start)->where('created_at', '<=', $end);
    }

    public function getTitle($jenis = '')
    {
        $aliases  = $this->aliases;

        return array_key_exists($jenis, $aliases) ? $aliases[$jenis] : ucfirst($jenis);
    }

    public function scopeTotalUang($query)
    {
        $sum = $query->sum('uang');

        if ($sum instanceof \Illuminate\Database\Eloquent\Builder)
            return 0;
            // return (integer) $sum;
    }

    public function scopeTotalBeras($query)
    {
        return (integer) $query->sum('beras');
    }

    public function scopeBerasFromUang($query, $harga_per_kilo)
    {
        if ($query->totalUang() instanceof Illuminate\Database\Eloquent\Builder)
            return 0;
            return $query->totalUang() / $harga_per_kilo;
    }   

    public function scopeTotalBerasWithUang($query, $harga_per_kilo)
    {
        $beras = 0;
        
        if ($query->berasFromUang($harga_per_kilo) instanceof Illuminate\Database\Eloquent\Builder)
            $beras += 0;
        else
            $beras += $query->berasFromUang($harga_per_kilo);

        if ($query->totalBeras() instanceof Illuminate\Database\Eloquent\Builder)
            $beras += 0;
        else
            $beras += $query->totalBeras();

        return $beras;
    }

}