<?php

namespace Scopes;

trait TahunanTrait
{
    public static function bootTahunanTrait()
    {
        static::addGlobalScope(new TahunanScope);
    }

    public static function semuaTahun()
    {        
        return with(new static)->newQueryWithoutScopes();
    }

    public function getGroupIdColumn()
    {
        return defined('static::GROUP_ID_COLUMN') ? static::PUBLISHED_COLUMN : 'group_id';
    }

    public function getQualifiedGroupIdColumn()
    {
        return $this->getTable().'.'.$this->getGroupIdColumn();
    }
}