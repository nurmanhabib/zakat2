<?php

namespace Scopes;

use Illuminate\Database\Query\Builder as BaseBuilder;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\ScopeInterface;
use Session;
use Group;

class TahunanScope implements ScopeInterface
{
    /**
     * Apply scope on the query.
     * 
     * @param \Illuminate\Database\Eloquent\Builder  $builder
     * @return void
     */
    public function apply(Builder $builder)
    {
        $tahun  = Session::get('tahun_zakat');
        $group  = Group::where('tahun', $tahun)->first();
        
        $column = $builder->getModel()->getQualifiedGroupIdColumn();
 
        $builder->where($column, $group->id);
 
        $this->addSemuaTahun($builder);
    }
 
    /**
     * Remove scope from the query.
     * 
     * @param  Builder $builder
     * @return void
     */
    public function remove(Builder $builder)
    {        
        $column = $builder->getModel()->getQualifiedGroupIdColumn();

        $query = $builder->getQuery();

        foreach ((array) $query->wheres as $key => $where)
        {
            // If the where clause is a soft delete date constraint, we will remove it from
            // the query and reset the keys on the wheres. This allows this developer to
            // include deleted model in a relationship result set that is lazy loaded.
            if ($this->isTahunanConstraint($where, $column))
            {
                unset($query->wheres[$key]);

                $query->wheres = array_values($query->wheres);
            }
        }
    } 

    /**
     * Extend Builder with custom method.
     * 
     * @param \Illuminate\Database\Eloquent\Builder  $builder
     */
    protected function addSemuaTahun(Builder $builder)
    {
        $builder->macro('semuaTahun', function(Builder $builder)
        {
            $this->remove($builder);
 
            return $builder;
        });
    }

    /**
     * Determine if the given where clause is a soft delete constraint.
     *
     * @param  array   $where
     * @param  string  $column
     * @return bool
     */
    protected function isTahunanConstraint(array $where, $column)
    {
        return $where['type'] == 'Basic' && $where['column'] == $column;
    }
}