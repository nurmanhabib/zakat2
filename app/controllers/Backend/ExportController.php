<?php
namespace Backend;

use App;
use View;
use Redirect;
use Input;
use Validator;

use Navigator;
use Button;
use Fpdf;
use Watermark;
use Excel;
use Carbon\Carbon as Carbon;
use Mysqldump;

use Zakat;

class ExportController extends BaseController {

    public function __construct()
    {
        parent::__construct();
    }

    public function getIndex()
    {
        Navigator::set_active(route('export'));
        
        return View::make('backend.export.index')
        ->with('page_title', 'Export Rekapitulasi');
    }

    public function getZakat($id, $copy = '')
    {
        $zakat  = Zakat::find($id);
        $group  = App::make('zakat')->group();

        if($copy == 'copy')
            $fpdf   = new Watermark();
        else
            $fpdf   = new Fpdf();

        $fpdf->AddPage('P', 'A4');
        $fpdf->SetMargins(10, 10);
        $fpdf->SetAutoPageBreak(false, 10);
        $fpdf->SetTitle($zakat->nomer);

        $fpdf->Image(public_path('img/'.$group->logo), 10, 10, 0, 15);
        $fpdf->SetFont('Arial', 'B', 12);
        $fpdf->Cell(30, 5);
        $fpdf->Cell(170, 5, 'PANITIA RAMADHAN '.$group->tahun.' H MASJID FARIDAN MURIDAN NOTO', 0, 1);
        $fpdf->Cell(30, 5);
        $fpdf->Cell(170, 5, 'SEKSI ZAKAT FITRAH', 0, 1);
        $fpdf->SetFont('Arial', 'B', 8);
        $fpdf->Cell(30, 5);
        $fpdf->Cell(170, 5, 'Jl. Beji 10 Yogyakarta 55112', 0, 1);

        $fpdf->SetLineWidth(0.5);
        $fpdf->Line(10, 26, 200, 26);

        $fpdf->SetFont('Arial', 'BU', 10);

        $fpdf->Ln(3);
        $fpdf->Cell(190, 5, 'TANDA TERIMA ' . strtoupper($zakat->title), 0, 1, 'C');

        $fpdf->SetFont('Arial', 'B', 10);
        $fpdf->Cell(95, 5, 'Nomer : ' . $zakat->nomer, 0, 0);
        $fpdf->SetFont('Arial', '', 10);
        $fpdf->Cell(95, 5, 'Tanggal/Jam : ' . $zakat->created_at->format('d-m-Y H:i:s'), 0, 1, 'R');

        $fpdf->Ln(3);
        $fpdf->Cell(28, 5, 'Telah terima dari', 0, 0);
        $fpdf->SetFont('Arial', 'B', 10);
        $fpdf->Cell(100, 5, $zakat->full_name_jiwa, 0, 1);

        $fpdf->SetFont('Arial', '', 10);
        $fpdf->Cell(50, 5, 'Alamat : ' . $zakat->alamat, 0, 1);

        $fpdf->Ln(5);
        $fpdf->SetFont('Arial', 'B', 12);
        $fpdf->Cell(190, 5, $zakat->title . ' berupa ' . $zakat->value_format, 0, 1, 'C');
        $fpdf->SetFont('Arial', 'I', 10);
        $fpdf->Cell(190, 5, '(' . ucfirst($zakat->terbilang) . ')', 0, 1, 'C');

        $fpdf->Ln(5);
        $fpdf->SetFont('Arial', '', 10);
        $fpdf->Cell(190, 5, 'Semoga amal ' . $zakat->prefix . ' diterima Allah SWT. Amin.', 0, 1);

        $fpdf->Ln(0);
        $fpdf->Cell(150, 5, '', 0, 0);
        $fpdf->Cell(50, 5, 'Panitia', 0, 1);

        $fpdf->Output($zakat->nomer . '.pdf', 'I');
        exit;
    }

    public function getBlank()
    {
        return View::make('backend.zakat.blank')
        ->with('page_title', 'Membuat Nota Kosong');
    }

    public function postBlank()
    {
        $group  = App::make('zakat')->group();

        $from   = Input::get('from', 1);
        $count  = Input::get('count', 10);
        $copy   = Input::get('copy', '');

        if($copy == 'copy')
            $fpdf   = new Watermark();
        else
            $fpdf   = new Fpdf();

        $i  = $from;

        for($r = 0; $r < $count; $r++)
        {
            $nomer    = 'CC';
            $nomer    .= date('_Ymd_');
            $nomer    .= sprintf('%03d', $i);

            $fpdf->SetMargins(10, 10);
            $fpdf->SetAutoPageBreak(false, 10);
            $fpdf->SetTitle($nomer);
            $fpdf->AddPage('P', 'A4');

            $fpdf->Image(public_path('img/'.$group->logo), 10, 10, 0, 15);
            $fpdf->SetFont('Arial', 'B', 12);
            $fpdf->Cell(30, 5);
            $fpdf->Cell(170, 5, 'PANITIA RAMADHAN '.$group->tahun.' H MASJID FARIDAN MURIDAN NOTO', 0, 1);
            $fpdf->Cell(30, 5);
            $fpdf->Cell(170, 5, 'SEKSI ZAKAT FITRAH', 0, 1);
            $fpdf->SetFont('Arial', 'B', 8);
            $fpdf->Cell(30, 5);
            $fpdf->Cell(170, 5, 'Jl. Beji 10 Yogyakarta 55112', 0, 1);

            $fpdf->SetLineWidth(0.5);
            $fpdf->Line(10, 26, 200, 26);

            $fpdf->SetFont('Arial', 'BU', 10);

            $fpdf->Ln(3);
            $fpdf->Cell(190, 5, 'TANDA TERIMA ZAKAT', 0, 1, 'C');

            $fpdf->SetFont('Arial', 'B', 10);
            $fpdf->Cell(95, 5, 'Nomer : ' . $nomer, 0, 0);
            $fpdf->SetFont('Arial', '', 10);
            $fpdf->Cell(95, 5, 'Tanggal/Jam : ........................................', 0, 1, 'R');

            $fpdf->Ln(3);
            $fpdf->Cell(28, 5, 'Telah terima dari', 0, 0);
            $fpdf->SetFont('Arial', 'B', 10);
            $fpdf->Cell(100, 5, 'bapak/ibu/sdr/i ........................................', 0, 1);

            $fpdf->SetFont('Arial', '', 10);
            $fpdf->Cell(50, 5, 'Alamat : ........................................', 0, 1);

            $fpdf->Ln(5);
            $fpdf->SetFont('Arial', 'B', 12);
            $fpdf->Cell(190, 5, 'Zakat berupa .............. ................', 0, 1, 'C');
            $fpdf->SetFont('Arial', 'I', 10);
            $fpdf->Cell(190, 5, '(.......................................................................)', 0, 1, 'C');

            $fpdf->Ln(5);
            $fpdf->SetFont('Arial', '', 10);
            $fpdf->Cell(190, 5, 'Semoga amal bapak/ibu/sdr/i diterima Allah SWT. Amin.', 0, 1);

            $fpdf->Ln(0);
            $fpdf->Cell(150, 5, '', 0, 0);
            $fpdf->Cell(50, 5, 'Panitia', 0, 1);
            $fpdf->Ln(5);

            $i++;
        }

        $fpdf->Output();
        exit;
    }

    public function getWatermark()
    {
        $pdf=new Watermark();
        $pdf->AddPage();
        $pdf->SetFont('Arial','',12);
        $txt="FPDF is a PHP class which allows to generate PDF files with pure PHP, that is to say ".
            "without using the PDFlib library. F from FPDF stands for Free: you may use it for any ".
            "kind of usage and modify it to suit your needs.\n\n";
        for($i=0;$i<25;$i++) 
            $pdf->MultiCell(0,5,$txt,0,'J');
        $pdf->Output();
        exit;
    }

    public function postRekapitulasi()
    {
        $rekapitulasi           = App::make('zakat')->rekapitulasi();
        $rekapitulasi->jenis    = Input::get('jenis');
        
        // Date Ranges
        if (Input::has('ranges')) {
            $ranges = Input::get('ranges');
            $parts  = explode(' - ', $ranges);
            
            $rekapitulasi->start_date   = date('Y-m-d 00:00:00', strtotime($parts[0]));
            $rekapitulasi->end_date     = date('Y-m-d 23:59:59', strtotime($parts[1]));
        }

        $rekapitulasi->toExcel();
    }

    public function getRekapitulasi($jenis = '')
    {
        if($jenis != '')
            $zakat  = Zakat::where('jenis', '=', $jenis)->get();
        else
            $zakat  = Zakat::all();

        if(!$zakat->count())
            return 'no data';

        return $this->generateExcel($jenis, $zakat);
    }

    public function getBackup()
    {
        Navigator::set_active(route('backup'));
        
        return View::make('backend.export.backup')
        ->with('page_title', 'Backup Database');
    }

    public function postBackup()
    {
        $dump   = new Mysqldump;
        $dump::table('zakat')->save(public_path('database/zakat2_' . date('Ymd_His') . '.sql'));

        return Redirect::route('backup')
        ->with('success', 'Database berhasil di backup.');
    }

    public function getLaporan()
    {
        return App::make('zakat')->laporan()->toPDF();
    }

}
