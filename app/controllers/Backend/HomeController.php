<?php

namespace Backend;

use App;
use Auth;
use View;
use Validator;
use Input;
use Redirect;
use Session;

use Navigator;

use Zakat;

class HomeController extends BaseController {

    public function __construct()
    {
        parent::__construct();
        
        Navigator::set_active(route('dashboard'));
    }

    public function index()
    {
        $zakat = App::make('zakat')->toArray();

        return View::make('backend.home.index')
        ->with('page_title', 'Dashboard')
        ->with('zakat', $zakat)
        ->with($zakat['ringkasan']);
    }

    public function error404()
    {
        return View::make('backend.error404');
    }

    public function getLogin()
    {
        return View::make('layout.login');
    }

    public function postLogin()
    {
        $rules  = array(
            'username'  => 'required|alpha_dash|exists:users',
            'password'  => 'required'
        );

        $validator  = Validator::make(Input::all(), $rules);

        if($validator->passes())
        {
            $username   = Input::get('username');
            $password   = Input::get('password');

            if(Auth::attempt(array('username' => $username, 'password' => $password)))
            {
                return Redirect::route('dashboard');
            }
            else
            {
                return Redirect::route('login')
                ->with(array('error' => 'Your login is invalid.'));
            }
        }
        else
        {
            return Redirect::route('login')
            ->withErrors($validator);
        }
    }

    public function getLogout()
    {
        Auth::logout();
        
        return Redirect::home();
    }

}