<?php

namespace Backend;

use Session;
use View;
use Carbon\Carbon;
use Zakat;
use Group;

class RekapitulasiController extends BaseController
{

    public function __construct()
    {
        parent::__construct();

        $tahun_aktif = Session::get('tahun_zakat');
        $this->group = Group::tahun($tahun_aktif);
    }

    public function getIndex()
    {
        $beras      = Zakat::fitrah()->beras()->get();
        $tot_beras  = 0.0;

        foreach($beras as $info)
            $tot_beras  += $info->beras;

        $uang       = Zakat::fitrah()->uang()->get();
        $tot_uang   = 0;

        foreach($uang as $info)
            $tot_uang   += $info->uang;

        $maal       = Zakat::maal()->uang()->get();
        $tot_maal   = 0;

        foreach($maal as $info)
            $tot_maal   += $info->uang;

        $shodaqoh       = Zakat::jenis('shodaqoh')->uang()->get();
        $tot_shodaqoh   = 0;

        foreach($shodaqoh as $info)
            $tot_shodaqoh   += $info->uang;

        $fidyah     = Zakat::jenis('fidyah')->uang()->get();
        $tot_fidyah = 0;

        foreach($fidyah as $info)
            $tot_fidyah += $info->uang;

        $data   = array(
            'page_title'        => 'Rekapitulasi Tahun ' . $this->group->tahun,
            'tahun'             => $this->group->tahun,
            'total_beras'       => $tot_beras,
            'total_uang'        => number_format($tot_uang, 0, ',', '.'),
            'total_beras_uang'  => number_format($tot_uang, 0, ',', '.'),
            'total_maal'        => number_format($tot_maal, 0, ',', '.'),
            'total_shodaqoh'    => number_format($tot_shodaqoh, 0, ',', '.'),
            'total_fidyah'      => number_format($tot_fidyah, 0, ',', '.'),
        );

        return View::make('backend.rekapitulasi.tahun', $data);
	}


}
