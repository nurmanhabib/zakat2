<?php

namespace Backend;

use View;
use Session;
use Redirect;
use Group;
use ZakatConfig;

class GroupController extends BaseController
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data = [
			'groups'	=> Group::all(),
		];

		return View::make('backend.group.index', $data)->withPageTitle('Groups');
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function changeTahun($tahun)
	{
		$group = Group::tahun($tahun);

		if ($group->count()) {
			$config = ZakatConfig::firstOrNew(['key' => 'tahun_zakat']);
			$config->value = $group->tahun;
			$config->save();

			return Redirect::back()->withSuccess('Tahun berhasil diganti.');
		} else {
			return Redirect::back()->withError('Tahun gagal diganti.');
		}
	}


}
