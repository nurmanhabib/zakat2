<?php

namespace Backend;

use View;
use Session;
use Auth;

use Button;
use Navigator;
use Breadcrumbs;

class BaseController extends \Controller {

    public function __construct()
    {
        $this->setupNavigator();
        $this->setupBreadcrumb();

        View::share('site_title', 'ZakatFMN ' . Session::get('tahun_zakat') . ' H');
        View::share('user', Auth::user());

        Button::controller(get_called_class());
    }

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        if ( ! is_null($this->layout))
        {
            $this->layout = View::make($this->layout);
        }
    }

    protected function setupNavigator()
    {
        $navs   = array(
            '<i class="fa fa-dashboard"></i> <span>Home</span>'                 => route('dashboard'),
            '<i class="fa fa-credit-card"></i> <span>Pembayaran Baru</span>'    => route('backend.zakat.create'),
            '<i class="fa fa-coffee"></i> <span>Rekapitulasi</span>'            => array(
                '<i class="fa fa-coffee"></i> <span>Semua</span>'               => route('backend.zakat'),
                '<i class="fa fa-download"></i> <span>Download</span>'          => route('export'),
            ),
            '<i class="fa fa-print"></i> <span>Cetak Nota Kosong</span>'        => route('invoice.blank'),
            '<i class="fa fa-print"></i> <span>Cetak Laporan</span>'            => route('laporan'),
            '<i class="fa fa-heart"></i> <span>Backup Database</span>'          => route('backup'),
            '<i class="fa fa-cog"></i> <span>Groups</span>'                     => route('backend.group.index'),
        );

        Navigator::initialize($navs, array(
            'ulattr'        => array('class' => 'sidebar-menu'),
            'liactive'      => 'active',
            'liparent_attr' => array('class' => 'treeview'),
            'child'         => array(
                'ulattr'        => array('class' => 'treeview-menu'),
                'liactive'      => 'active'
            )
        ));
    }

    protected function setupBreadcrumb()
    {
        // Home
        Breadcrumbs::register('home', function($breadcrumbs)
        {
            $breadcrumbs->push('Home', route('home'));
        });
    }

}