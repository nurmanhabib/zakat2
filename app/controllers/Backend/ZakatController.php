<?php
namespace Backend;

use View;
use Redirect;
use Input;
use Validator;

use Navigator;
use Button;
use Breadcrumbs;

use Zakat;

class ZakatController extends BaseController {

    public function __construct()
    {
        parent::__construct();

        Breadcrumbs::register('backend.zakat', function($breadcrumbs)
        {
            $breadcrumbs->parent('home');
            $breadcrumbs->push('Zakat', route('backend.zakat'));
        });

        Breadcrumbs::register('backend.zakat.create', function($breadcrumbs)
        {
            $breadcrumbs->parent('backend.zakat');
            $breadcrumbs->push('Pembayaran Zakat', route('backend.zakat.create'));
        });

        Breadcrumbs::register('backend.zakat.show', function($breadcrumbs, $id)
        {
            $zakat = Zakat::find($id);

            $breadcrumbs->parent('backend.zakat');
            $breadcrumbs->push($zakat->nomer, route('backend.zakat.show', $zakat->id));
        });

        Breadcrumbs::register('backend.zakat.edit', function($breadcrumbs, $id)
        {
            $zakat = Zakat::find($id);

            $breadcrumbs->parent('backend.zakat');
            $breadcrumbs->push($zakat->nomer, route('backend.zakat.edit', $zakat->id));
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        Navigator::set_active(route('backend.zakat'));

        $zakat  = Zakat::all();

        return View::make('backend.zakat.index')
        ->with('page_title', 'Rekapitulasi Zakat Fitrah/Maal')
        ->with('zakat', $zakat);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        Navigator::set_active(route('backend.zakat.create'));

        return View::make('backend.zakat.create')
        ->with('page_title', 'Pembayaran Zakat Fitrah/Maal');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $rules  = array(
            'nama'          => 'required',
            'alamat'        => 'required',
            'jenis_bayar'   => 'required',
        );

        $validator  = Validator::make(Input::all(), $rules);

        if($validator->passes())
        {
            $zakat               = new Zakat;
            $zakat->prefix       = Input::get('prefix');
            $zakat->nama         = Input::get('nama');
            $zakat->alamat       = Input::get('alamat');
            $zakat->rt           = Input::get('rt');
            $zakat->jumlah_jiwa  = Input::get('jumlah_jiwa');
            $zakat->jenis        = Input::get('jenis');

            $jenis_bayar         = Input::get('jenis_bayar');

            if($jenis_bayar == 'beras')
                $zakat->beras    = Input::get('beras');
            else
                $zakat->uang     = Input::get('uang');

            $zakat->keterangan   = Input::get('keterangan');
            $zakat->save();

            return Redirect::route('backend.zakat.show', array($zakat->id))
            ->with('success', 'Transaksi Zakat berhasil dilakukan');
        }
        else
        {
            return Redirect::route('backend.zakat.create')
            ->withErrors($validator)
            ->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        Navigator::set_active(route('backend.zakat'));

        $zakat  = Zakat::find($id);

        return View::make('backend.zakat.show')
        ->with('page_title', $zakat->nomer . ' Edit Transaksi Zakat')
        ->with('zakat', $zakat);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        Navigator::set_active(route('backend.zakat'));

        $zakat  = Zakat::find($id);

        return View::make('backend.zakat.edit')
        ->with('page_title', $zakat->nomer)
        ->with('zakat', $zakat);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        // Jika digunakan sebagai transaksi baru
        if(Input::has('as_new'))
        {
            return $this->store();
        }

        $rules  = array(
            'nama'          => 'required',
            'alamat'        => 'required',
            'jenis_bayar'   => 'required',
        );

        $validator  = Validator::make(Input::all(), $rules);

        if($validator->passes())
        {
            $zakat               = Zakat::find($id);
            $zakat->prefix       = Input::get('prefix');
            $zakat->nama         = Input::get('nama');
            $zakat->alamat       = Input::get('alamat');
            $zakat->rt           = Input::get('rt');
            $zakat->jumlah_jiwa  = Input::get('jumlah_jiwa');
            $zakat->jenis        = Input::get('jenis');

            $jenis_bayar         = Input::get('jenis_bayar');

            if($jenis_bayar == 'beras')
            {
                $zakat->beras    = Input::get('beras');
                $zakat->uang     = 0;
            }
            else
            {
                $zakat->beras    = 0;
                $zakat->uang     = Input::get('uang');
            }

            $zakat->keterangan   = Input::get('keterangan');
            $zakat->save();

            return Redirect::route('backend.zakat.edit', array($id))
            ->with('success', 'Informasi Zakat Fitrah berhasil diperbarui.');
        }
        else
        {
            return Redirect::route('backend.zakat.edit', array($id))
            ->withErrors($validator)
            ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $zakat  = Zakat::find($id);
        $zakat->delete();

        return Redirect::route('backend.zakat')
        ->with('success', 'Transaksi zakat berhasil dihapus');
    }

}